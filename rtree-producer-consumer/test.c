
#include "index.h"
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <inttypes.h>

#define NANOS ((unsigned long)1000000000)

//função do código original, não usada neste trabalha
//mantida para possível uso futuro
int MySearchCallback(int id, void* arg)
{
  return 1; // keep going
}


int main(int argc, char **argv){
  //dois parâmetros de entrada
  //1: quantidade de dados
  //2: quantidade de threads
  if(argc!=3){
    printf("digite o tamanho do vetor e o número de threads\n");
    exit(1);
  }

  int n_rects = atoi(*(argv+1));
  MAXTHR = atoi(*(argv+2));

  if(n_rects<=0){
    printf("input invalido\n" );
    exit(1);
  }

  struct Rect *rects = malloc(sizeof(struct Rect) * n_rects);
  //n_search1~5 a quantidade de dados encontrados nas pesquisas
  //t_search1~5 o tempo gasto por cada pesquisa
  int i, n_search1,n_search3,n_search4,n_search5;
  double t_search1, t_search3,t_search4,t_search5;
  threads = 0;
  //node raiz
  struct Node* root = RTreeNewIndex();

  //GERADOR DOS VETORES
  for(i=0;i<n_rects;i++){
    int j;
    for(j=0;j<NUMSIDES;j++){
      if(j<NUMDIMS)
      rects[i].boundary[j]=i;
      else
      rects[i].boundary[j]=i+1;
    }

  }

  //vetor usado para parar os threads
  kill = malloc(sizeof(struct Node));
  kill->level=-1;

  //inserindo os dados na árvore a partir da raiz
  for(i=0;i<n_rects;i++)
  RTreeInsertRect(&rects[i],i+1,&root,0);

  //rect a ser pesquisado
  struct Rect rect_search ;
  for(i=0;i<NUMSIDES/2;i++)
    rect_search.boundary[i]=0;

  for(i=NUMSIDES/2;i<NUMSIDES;i++)
    rect_search.boundary[i]=n_rects+1;

  //RTREESEARCH

  uint64_t diff;
  struct timespec tick, tock;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tick);
  n_search1 = RTreeSearch(root, &rect_search, MySearchCallback, 0);

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tock);
  diff = NANOS * (tock.tv_sec - tick.tv_sec) + tock.tv_nsec - tick.tv_nsec;
  t_search1 =  (double)diff/NANOS;

  //RTREESEARCH3
  struct Search s_search3;
  s_search3.N=root;
  s_search3.R=&rect_search;
  s_search3.shcb=&MySearchCallback;
  s_search3.cbarg=NULL;
  n_search3=0;
  s_search3.hits = &n_search3;

  pthread_t thread_main;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tick);
  pthread_create(&thread_main,NULL,RTreeSearch3,&s_search3);
  pthread_join(thread_main,NULL);
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tock);
  diff = NANOS * (tock.tv_sec - tick.tv_sec) + tock.tv_nsec - tick.tv_nsec;
  t_search3 =  (double)diff/NANOS;

  //RTREESEARCH4
  pthread_t thre[MAXTHR];
  struct Queue* q;
  QueueInit(&q);
  q->active= MAXTHR;
  struct Search s_search4[MAXTHR];
  n_search4=0;
  for(i=0;i<MAXTHR;i++){
    s_search4[i].N=root;
    s_search4[i].R=&rect_search;
    s_search4[i].shcb=&MySearchCallback;
    s_search4[i].queue= &q;
    s_search4[i].cbarg=NULL;
    s_search4[i].hits=&n_search4;
    s_search4[i].flag=0;
    s_search4[i].depth = 0;
  }
  s_search4[0].flag=1;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tick);
  for(i=0;i<MAXTHR;i++){
    pthread_create(&thre[i],NULL,RTreeSearch4,&s_search4[i]);
  }
  for(i=0;i<MAXTHR;i++)
  pthread_join(thre[i],NULL);
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tock);
  diff = NANOS * (tock.tv_sec - tick.tv_sec) + tock.tv_nsec - tick.tv_nsec;
  t_search4 =  (double)diff/NANOS;

  //RTREESEARCH5
  uint64_t diff5;
  struct timespec tick5, tock5;
  Data s_search5[MAXTHR];
  n_search5=0;
  q->active= MAXTHR;
  q->inactive =0;
  for(i=0;i<MAXTHR;i++){
    s_search5[i].node=NULL;
    s_search5[i].rect = &rect_search;
    s_search5[i].queue = &q;
    s_search5[i].hits = &n_search5;
    pthread_create(&thre[i],NULL,InitThread,&s_search5[i]);
  }
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tick5);
  QueuePush(&q,root);
  for(i=0;i<MAXTHR;i++){pthread_join(thre[i],NULL);}
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tock5);
  diff5 = NANOS * (tock5.tv_sec - tick5.tv_sec) + tock5.tv_nsec - tick5.tv_nsec;
  t_search5 =  (double)diff5/NANOS;
  //prints para ver a quantidade de dados encontrada e o tempo de cada pesquisa
  //printf("RTREESEARCH: %d | RTREESEARCH3: %d| RTREESEARCH4: %d| RTREESEARCH5: %d\n",n_search1,n_search3,n_search4,n_search5);
  //printf("RTREESEARCH: %.6lf | RTREESEARCH3: %.6lf| RTREESEARCH4: %.6lf| RTREESEARCH5: %.6lf\n", t_search1, t_search3,t_search4,t_search5 );

  //print que imprime apenas o tempo
  printf("%.6lf\t%.6lf\t%.6lf\t%.6lf\n", t_search1, t_search3,t_search4,t_search5 );
  QueueKill(&q);

  return 0;
}
